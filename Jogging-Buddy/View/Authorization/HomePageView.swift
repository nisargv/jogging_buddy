//
//  HomePageView.swift
//  Jogging-Buddy
//
//  Created by Nisarg on 2021-12-01.
//

import SwiftUI

struct HomePageView: View {
//    @ObservedObject var locationManager = LocationManager()
    @Environment(\.presentationMode) var presentationMode
    

    @EnvironmentObject var userSettings: UserSettings
    @EnvironmentObject() var userViewModel: UserViewModel
    @State private var purpose: String = ""
    @State private var meetingDate = Date()
    @State private var meetingLocation: String = ""
    @State private var duration = 0
    @State var durationType = ["Private","Public"]
    
    @State private var meetingLat: Double = 0.0
    @State private var meetingLng: Double = 0.0
   

struct HomePageView_Previews: PreviewProvider {
    static var previews: some View {
        HomePageView()
    }
}

}
