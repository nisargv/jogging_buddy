//
//  AppDelegate.swift
//  Jogging-Buddy
//
//  Created by Nisarg on 2021-11-14.
//

import UIKit
import CoreData
import Firebase
import GoogleSignIn
import GooglePlaces


@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  
    lazy var firestore = Firestore.firestore()
    lazy var functions = Functions.functions()
    lazy var auth = Auth.auth()
    lazy var storage = Storage.storage()

    
    lazy var googleDelegate = {
        return GoogleDelegate(firebaseAuth: self.auth)
    }()
    

    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        // Override point for customization after application launch.
        
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = googleDelegate
        GMSPlacesClient.provideAPIKey("AIzaSyCvPy4iKLh_kzHHhVjpUGun3PuUlX4g2xA ")

        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
}

