//
//  TrackList.swift
//  Jogging-Buddy
//
//  Created by Nisarg on 2021-11-14.
//

import SwiftUI

struct TrackList: View {
    @EnvironmentObject() var userViewModel: UserViewModel
    @EnvironmentObject() var authViewModel: AuthViewModel
    @State private var name: String = ""
    @State private var tapped = false
    @State private var tapped2 = false
    @State private var selection: Int? = nil
    @State private var selectedIndex : Int? = nil
    var body: some View {
        NavigationView{
            VStack{
                NavigationLink(destination: ProfileView(), tag:1, selection: $selection){}
            List{
            
               
                    
                    HStack{
                        Text("Track Name")
                        Spacer()
                        Text("Distance")
                    }
                HStack{
                    Text("Track 2")
                    Spacer()
                    Text("3 KM")
                }
                HStack{
                    Text("Track 3")
                    Spacer()
                    Text("5 KM")
                }
                
            }//List
            }//Vstack
            .toolbar{
                ToolbarItem(placement: .navigationBarTrailing){
                    Menu{
                        //Button("Delete Account", action: self.deleteAccount)
                        Button("Edit Profile", action: self.editProfile)
                        Button("SignOut", action: self.signOut)
                    }label:{
                        Image(systemName: "gear")
                    }

                }
            }
            .navigationBarTitle("Track List", displayMode: .inline)
            .navigationBarBackButtonHidden(true)

}// Navigationview
            }
    private func signOut(){
        self.authViewModel.signOut()
        
    }
    private func editProfile(){
//        print(#function)
//        self.userViewModel.userProfile
        self.selection = 1
       
    }
}


struct TrackList_Previews: PreviewProvider {
    static var previews: some View {
        TrackList()
    }
}
