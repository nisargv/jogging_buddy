//
//  UserViewModel.swift
//  Jogging-Buddy
//
//  Created by Baljinder Singh on 2021-11-27.
//

import Foundation
import Firebase

struct UserData: Codable {
    let username: String
    let email: String
    let password: String
    
    init(username: String, email: String, password: String) {
        self.username = username
        self.email = email
        self.password = password
    }
    
    enum CodingKeys: String, CodingKey {
        case username
        case email
        case password
    }
}
enum UserCreationError: Error {
    case usernameExists
    case emailExists
    case other
}
class UserProfile: ObservableObject {
    @Published var username: String
    @Published var email: String
    
    init(username: String, email: String) {
        self.username = username
        self.email = email
    }
}
class UserViewModel: ObservableObject {
    private let firestore: Firestore
    private let functions: Functions
    private let auth: Auth
    private let storage: Storage
    private var userListener: ListenerRegistration? = nil
    private var authListener: AuthStateDidChangeListenerHandle? = nil
    private var user: User? = nil

    private func listenToUserWithId(userId: String) {
        self.userListener = self.firestore.collection("users").document(userId)
            .addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("Error fetching document: \(error!)")
                    return
                }
                
                // Iggnoring when the document is empty
                if !document.exists {
                    return
                }

                try? self.user = document.data(as: User.self)
                
                if let user = self.user {
                    self.profileIsLoaded = true

                    self.userProfile = UserProfile(
                        username: user.username,
                        email: user.email
                    )
                }
            }
    }
    init(firestore: Firestore, functions: Functions, auth: Auth, storage: Storage) {
        self.firestore = firestore
        self.functions = functions
        self.auth = auth
        self.storage = storage
    }
    @Published var profileIsLoaded: Bool = false
    @Published var userProfile: UserProfile = UserProfile(username: "", email: "")
    
    
    public func createUser(data: UserData, image: Data?, completion: @escaping (UserCreationError?)->()) {
        let functionData = ["email": data.email, "username": data.username, "password": data.password]
        
        
        self.auth.createUser(withEmail: data.email, password: data.password) {  authResult, error in
            // [START_EXCLUDE]
            print("User has been created \(data.email) & \(data.password)")
            // [END_EXCLUDE]
            
          }
        
        }
        
//        self.functions.httpsCallable("createAccount").call(functionData) { (result, error) in
//            if let error = error as NSError? {
//                if error.localizedDescription == "email" {
//                    completion(UserCreationError.emailExists )
//                } else if error.localizedDescription == "username" {
//                    completion(UserCreationError.usernameExists)
//                } else {
//                    completion(UserCreationError.other)
//                }
//
//                return
//            }
            
//            guard let uid = result?.data as? String else {
//                completion(UserCreationError.other)
                
//                return
//            }
            
            
            
            // TODO: Think about NOT waiting for the upload to finish
            
//          }
//    }
    public func listenToUser() {
        self.profileIsLoaded = false
        
        self.authListener = self.auth.addStateDidChangeListener { (auth, user) in
            self.userListener?.remove()
            
            if let user = user {
                self.listenToUserWithId(userId: user.uid)
            } else {
                self.user = nil
                self.userProfile = UserProfile(username: "", email: "")
            }
        }
    }
    public func stopListeningToUser() {
        if let listener = self.authListener {
            self.auth.removeStateDidChangeListener(listener)
        }
    }
    
    public func changeUsername(newUsername: String, completion: @escaping (Error?) -> Void) {
        let uid: String = (self.user?.id!)!
        
        // For instant update
        self.userProfile.username = newUsername
        
        self.firestore.collection("users")
            .document(uid)
            .updateData([
                "username": newUsername
            ], completion: completion)
    }
    
    public func changePassword(newPassword: String, completion: @escaping (Error?) -> Void) {
        self.auth.currentUser?.updatePassword(to: newPassword, completion: completion)
    }
}
